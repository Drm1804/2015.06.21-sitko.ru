var filmApp = angular.module('filmApp', ["ngRoute"])
    .config(function($routeProvider){
        $routeProvider
            .when("/",{
                templateUrl: "template/search.html"
            })
            .when("/item/:itemId", {
                templateUrl: "template/item.html"
            })
            .when("/edit", {
                templateUrl: "template/edit-item.html"
            })
            .otherwise({
                tedirrectTo:"/"
            })
    });

//Модель

var data = {
    "page": 1,
    "results": [],
    "total_pages": 0,
    "total_results": 0
};

var itemFilmData = {
    original_title: "Название фильма",
    release_date: "2015-06-22",
    overview: "Описание фильма",
    poster_path: ""
};

var credits = {
    "actors": []
}
//Контроллеры

filmApp.controller('searchFilmCtrl', function($scope){
    $scope.datas = data.results;

    $scope.totalResul = data.total_results;

    $scope.itemId = data.id;

    $scope.searchFilms = function(){
        function successCB(APIres) {
            APIres = JSON.parse(APIres);
            $scope.setFilms(APIres)
        };
        function errorCB(APIres) {
            console.log("Error callback: " + APIres);
        };
        theMovieDb.search.getMovie({"query": $scope.searchName }, successCB, errorCB)

    };
    $scope.datas = data.results;
    $scope.setFilms = function(APIres){

        $scope.datas = APIres.results;
        $scope.totalResul = APIres.total_results;
        $scope.$$phase || $scope.$apply();
    }
});

filmApp.controller('itemFilmCtrl', function($scope){

    $scope.filmName = itemFilmData.original_title;
    $scope.filmDate = itemFilmData.release_date;
    $scope.filmDescript = itemFilmData.overview;
    $scope.filmposter = itemFilmData.poster_path;
    $scope.getInfoAboutFilm = function(){
        function successCB(APIres){
            APIres = JSON.parse(APIres);
            itemFilmData = APIres;
            $scope.setInfo(itemFilmData)
            console.log(itemFilmData);
        }

        function errorCB(APIres){
            console.log("Error callback: " + APIres);
        }

        theMovieDb.movies.getById({"id": ''+splitHref[1]+'' }, successCB, errorCB)
    }();

    $scope.setInfo = function(APIres){
        $scope.filmName = APIres.original_title;
        $scope.filmDate = itemFilmData.release_date;
        $scope.filmDescript = itemFilmData.overview;
        $scope.filmposter = itemFilmData.poster_path;
        $scope.$$phase || $scope.$apply();
    };


});

filmApp.controller('actorFilmCtrl', function($scope){

    $scope.actors = credits.actors;

    $scope.getListActors = function(){
        function successCB(APIres){
            console.log(APIres);
            APIres = JSON.parse(APIres);
            $scope.setActorFilm(APIres);

        }

        function errorCB(APIres){
            console.log("Error callback: " + APIres);
        }

        theMovieDb.movies.getCredits({"id": ''+splitHref[1]+'' }, successCB, errorCB)
    }();
    $scope.setActorFilm = function(APIres){
        $scope.actors = APIres.cast;
        console.log($scope);
        $scope.$$phase || $scope.$apply();

    }

});
filmApp.controller('editFilmCtrl', function($scope){
    $scope.filmName = itemFilmData.original_title;
    $scope.filmDate = itemFilmData.release_date;
    $scope.filmDescript = itemFilmData.overview;
    $scope.filmposter = itemFilmData.poster_path;
});